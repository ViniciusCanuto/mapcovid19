import requests
import json
from flask import Flask, render_template, redirect, url_for
from flask_cors import CORS

app = Flask(__name__)
cors = CORS(app)

@app.route("/", methods=['GET'])
def index():
    return render_template('index.html')

if __name__ == '__main__':
    app.run(debug=True)
