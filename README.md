# __Projeto Covid-19 & Vacina BCG__

    
## Descrição:
    O projeto consiste na utilização de dados da 
    World Health Organization(WHO) e BCG Atlas na
    criação de uma View relacionando os países que
    adotaram a política BCG e com os casos de Covid19.
    
    Criamos uma simples API para resgatar os dados da
    BCG e WHO e alimentar a View.
    

## __Equipe:__
### - Beatriz Veras
__[Linkedin](https://www.linkedin.com/in/beatriz-veras-118222175/)__
_________
__[Gitlab](https://gitlab.com/biaveras)__
### - Gabriel Santos
__[Linkedin](https://www.linkedin.com/in/gabrielssg/)__
_________
__[Gitlab](https://gitlab.com/santosgsg)__
### - Víctor Masashi
__[Linkedin](https://www.linkedin.com/in/victor-masashi-ofuji-b72099179/)__
_________
__[Gitlab](https://gitlab.com/victor.masashio)__
### - Vinícius Canuto
__[Linkedin](https://www.linkedin.com/in/vin%C3%ADcius-de-carvalho-canuto-749602170/)__
_________
__[Gitlab](https://gitlab.com/ViniciusCanuto)__

## Links de Referencia:
- __[BCG ATLAS]__ http://www.bcgatlas.org/
- __[WHO]__ https://dashboards-dev.sprinklr.com/


## Projetos utilizados para a criação da View
___________________________________________
__[JvectorMap](https://jvectormap.com/)__
___________________________________________
__[API](https://api-coronavirus-bcg.herokuapp.com/)__
